s="Twinkle, twinkle, little star, How I wonder what you are! Up above the world so high, " \
  "Like a diamond in the sky. Twinkle, twinkle, little star, How I wonder what you are"
print(s.index(','))
print(s[0:s.index('How')])
print("   ",s[s.index('How'):s.index('Up')])
print(s[s.index('Up'):s.index('Like')])
print("   ",s[s.index('Like'):s.index(' Twinkle')])
print(s[0:s.index('How')])
print("   ",s[s.index('How'):s.index('! Up')])