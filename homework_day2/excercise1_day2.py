1.	Given the following statement: u, v, x, y, z = 29, 12, 10, 4, 3.
RESULT:
a.	u / v =  2.8333333333333335
b.	t = (u == v)  ⇒ t = False
c.	u % x = 9
d.	t = (x >= y)  ⇒ t = True
e.	u += 5  ⇒ u = 34
f.	u %=z ⇒ u = 1
g.	t = (v > x and y < z)  ⇒ t = False 
h.	x**z = 1000
i.	x // z = 3

